/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
 * Code: https://bitbucket.org/rafassmail/cursospring
 */
package com.rafasoriazu.ejercicio02JPA.dao.impl;


import java.util.List;

import org.springframework.stereotype.Component;

import com.rafasoriazu.ejercicio02JPA.dao.ContactDao;
import com.rafasoriazu.ejercicio02JPA.model.Contact;


/**
 * 
 * @author Rafa Soriazu (rafasoriazu@gmail.com)
 *
 */
@Component
public class ContactDaoImpl extends GenericDaoImpl<Contact> implements ContactDao {

	@Override
	public List<Contact> getContactsByEmail(String email) {
		return em
				.createNamedQuery("findContactByEmail")
				.setParameter("email", "%" + email + "%")
				.getResultList();
	}

	
   
}