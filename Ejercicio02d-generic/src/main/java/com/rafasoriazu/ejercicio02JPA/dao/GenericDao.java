/* 
 * Copyright (c) https://gist.github.com/picodotdev/6039493#file-genericdao-java
 */
package com.rafasoriazu.ejercicio02JPA.dao;

import java.util.List;


import com.rafasoriazu.ejercicio02JPA.model.Contact;



/*
 * Rafa Soriazu (rafasoriazu@gmail.com)
 */

public interface GenericDao<T> {
	    
	    List<T> list();

	    T create(T t);

	    void delete(Object id);

	    T find(Object id);

	    T update(T t);   
}
