package com.rafasoriazu;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Message {
	private String message;

	@Bean
    public Message myFavouriteBean() {
		Message message= new Message();
		message.setMessage("Creado con anotaciones");
		return message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void printMessage(){
		System.out.print("Mensaje: "+message);
	}
}
