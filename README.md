# Repositorio con los ejemplos del curso introducción a Spring Framework  #

Spring Framework es una plataforma de desarrollo que nos proporciona una infraestructura para desarrollar aplicaciones empresariales Java. Spring maneja toda la infraestructura permitiendo al programador centrarse en la aplicación, es el “pegamento” que une todos sus componentes, maneja su ciclo de vida y la interacción entre ellos. 

En este taller se busca enseñar cuáles son las características y el alcance de este framework, conceptos básicos en Spring como IoC o AOP y cómo configurar y programar una aplicación. Durante el curso se practicarán algunos ejemplos acerca de cómo emplear módulos de Spring como: JPA/Hibernate, Spring MVC, Spring Security, REST/SOAP Web Services…

Este curso está dirigido a desarrolladores y analistas con conocimientos de Java que quieran conocer y practicar las funcionalidades y características de Spring 4.


## Tema 2 ##

* Persistencia, ORMs.
* * Acceso a base datos mediante JDBC
* * Transacciones
* * ORM basics: JPA, Hibernate



### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact