package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the project_status_type database table.
 * 
 */
@Entity
@Table(name="project_status_type")
@NamedQuery(name="ProjectStatusType.findAll", query="SELECT p FROM ProjectStatusType p")
public class ProjectStatusType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String description;

	private String guidelines;

	private String name;

	//bi-directional many-to-one association to Project
	@OneToMany(mappedBy="projectStatusType")
	private List<Project> projects;

	public ProjectStatusType() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGuidelines() {
		return this.guidelines;
	}

	public void setGuidelines(String guidelines) {
		this.guidelines = guidelines;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Project addProject(Project project) {
		getProjects().add(project);
		project.setProjectStatusType(this);

		return project;
	}

	public Project removeProject(Project project) {
		getProjects().remove(project);
		project.setProjectStatusType(null);

		return project;
	}

}