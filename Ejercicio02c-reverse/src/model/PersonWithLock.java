package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the person_with_lock database table.
 * 
 */
@Entity
@Table(name="person_with_lock")
@NamedQuery(name="PersonWithLock.findAll", query="SELECT p FROM PersonWithLock p")
public class PersonWithLock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="first_name")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	@Column(name="sys_timestamp")
	private Timestamp sysTimestamp;

	public PersonWithLock() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getSysTimestamp() {
		return this.sysTimestamp;
	}

	public void setSysTimestamp(Timestamp sysTimestamp) {
		this.sysTimestamp = sysTimestamp;
	}

}