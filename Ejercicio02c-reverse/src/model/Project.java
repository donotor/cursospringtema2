package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the project database table.
 * 
 */
@Entity
@NamedQuery(name="Project.findAll", query="SELECT p FROM Project p")
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private BigDecimal budget;

	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;

	private String name;

	private BigDecimal spent;

	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;

	//bi-directional many-to-one association to Milestone
	@OneToMany(mappedBy="project")
	private List<Milestone> milestones;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="manager_person_id")
	private Person person;

	//bi-directional many-to-one association to ProjectStatusType
	@ManyToOne
	@JoinColumn(name="project_status_type_id")
	private ProjectStatusType projectStatusType;

	//bi-directional many-to-many association to Project
	@ManyToMany
	@JoinTable(
		name="related_project_assn"
		, joinColumns={
			@JoinColumn(name="project_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="child_project_id")
			}
		)
	private List<Project> projects1;

	//bi-directional many-to-many association to Project
	@ManyToMany(mappedBy="projects1")
	private List<Project> projects2;

	//bi-directional many-to-many association to Person
	@ManyToMany
	@JoinTable(
		name="team_member_project_assn"
		, joinColumns={
			@JoinColumn(name="project_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="person_id")
			}
		)
	private List<Person> persons;

	public Project() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getBudget() {
		return this.budget;
	}

	public void setBudget(BigDecimal budget) {
		this.budget = budget;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSpent() {
		return this.spent;
	}

	public void setSpent(BigDecimal spent) {
		this.spent = spent;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public List<Milestone> getMilestones() {
		return this.milestones;
	}

	public void setMilestones(List<Milestone> milestones) {
		this.milestones = milestones;
	}

	public Milestone addMilestone(Milestone milestone) {
		getMilestones().add(milestone);
		milestone.setProject(this);

		return milestone;
	}

	public Milestone removeMilestone(Milestone milestone) {
		getMilestones().remove(milestone);
		milestone.setProject(null);

		return milestone;
	}

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public ProjectStatusType getProjectStatusType() {
		return this.projectStatusType;
	}

	public void setProjectStatusType(ProjectStatusType projectStatusType) {
		this.projectStatusType = projectStatusType;
	}

	public List<Project> getProjects1() {
		return this.projects1;
	}

	public void setProjects1(List<Project> projects1) {
		this.projects1 = projects1;
	}

	public List<Project> getProjects2() {
		return this.projects2;
	}

	public void setProjects2(List<Project> projects2) {
		this.projects2 = projects2;
	}

	public List<Person> getPersons() {
		return this.persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

}