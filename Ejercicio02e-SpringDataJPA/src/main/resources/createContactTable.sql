CREATE TABLE contact(id INTEGER generated by default as identity (start with 1) not null,
			    email VARCHAR(20) NOT NULL,
				name VARCHAR(20) NOT NULL,
			    PRIMARY KEY (ID)); 