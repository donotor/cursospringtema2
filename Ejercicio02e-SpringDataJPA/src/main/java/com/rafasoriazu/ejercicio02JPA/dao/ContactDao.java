/* 
 * Copyright (c) https://gist.github.com/picodotdev/6039493#file-genericdao-java
 */
package com.rafasoriazu.ejercicio02JPA.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.rafasoriazu.ejercicio02JPA.model.Contact;



/*
 * Rafa Soriazu (rafasoriazu@gmail.com)
 */

public interface ContactDao extends CrudRepository<Contact, Long> {
	
	List<Contact> findByEmailLike(String email);
}
