/* 
 * Copyright (c) Curso Introducción Spring Famework2015 
  * Code: https://bitbucket.org/rafassmail/cursospring
 */

package com.rafasoriazu.example_jdbc.model;

/**
 * Contact model class
 * 
 * @author: Rafael Soriazu (rafasoriazu@gmail.com)
 */

public class Contact {

	private Long id;
	private String email;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
